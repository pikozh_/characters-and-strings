import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;

import java.util.Arrays;

public class ThirdParameterizedTest {

    // testing method calculateLengthOfTheShortestWord()
    @RunWith(Parameterized.class)
    public static class calculateLengthOfTheShortestWordParamTest{
        private String inputLine;
        private int expectedValue;

        public calculateLengthOfTheShortestWordParamTest(String inputLine, int expectedValue){
            this.inputLine = inputLine;
            this.expectedValue = expectedValue;
        }

        @Parameterized.Parameters(name = "ParameterizedTest")
        public static Iterable<Object[]> valuesForTest(){
            return Arrays.asList(new Object[][]{
                    {"Hello, how are you?", 3},
                    {"My name is Franko", 2},
                    {"AkunaMatata",11}
            });
        }

        @Test
        public void calculateLengthOfTheShortestWordParamTestPass(){
            Assert.assertEquals(expectedValue, new Third().calculateLengthOfTheShortestWord(inputLine));
        }
    }


    // testing method spaceAfterPunctuation()

    @RunWith(Parameterized.class)
    public static class spaceAfterPunctuationParamTest{
        private String inputLine;
        private String expectedLine;

        public spaceAfterPunctuationParamTest(String inputLine, String expectedLine){
            this.inputLine = inputLine;
            this.expectedLine = expectedLine;
        }

        @Parameterized.Parameters(name = "ParameterizedTest")
        public static Iterable<Object[]> valuesForTest(){
            return Arrays.asList(new Object[][]{
                    {"Hello,how are, you?", "Hello, how are, you?"},
                    {"My,name,is,Franko","My, name, is, Franko"},
                    {"AkunaMatata","AkunaMatata"}
            });
        }

        @Test
        public void calculateLengthOfTheShortestWordParamTestPass(){
            Assert.assertEquals(expectedLine, new Third().spaceAfterPunctuation(inputLine));
        }
    }


    // testing method reverseString()

    @RunWith(Parameterized.class)
    public static class reverseStringParamTest{
        private String inputLine;
        private String expectedLine;

        public reverseStringParamTest(String inputLine, String expectedLine){
            this.inputLine = inputLine;
            this.expectedLine = expectedLine;
        }

        @Parameterized.Parameters(name = "ParameterizedTest")
        public static Iterable<Object[]> valuesForTest(){
            return Arrays.asList(new Object[][]{
                    {"abcde", "edcba"},
                    {"12345","54321"},
                    {"abc123","321cba"}
            });
        }

        @Test
        public void reverseStringParamTestPass(){
            Assert.assertEquals(expectedLine, new Third().reverseString(inputLine));
        }
    }
}
