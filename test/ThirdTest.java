import org.junit.Assert;
import org.junit.Test;

public class ThirdTest {

    Third thirdInstance = new Third();

    @Test
    public void calculateLengthOfTheShortestWordPass() {

        //when
        int actual = thirdInstance.calculateLengthOfTheShortestWord("The length of the shortest word in the line, with spaces and commas.");
        int expected = 2;

        //then
        Assert.assertEquals(expected, actual);
    }

    @Test
    public void replaceLastThreeCharactersWith$Pass() {
        //given
        String[] str = new String[]{"aasd", "asknf", "das", "as", "dasdas", "da", "sdas", "dasdas", "das", "das", "dakls", "adukr"};

        //when
        String[] actual = thirdInstance.replaceLastThreeCharactersWith$(str,5);
        String[] expected = new String[]{"aasd", "as$", "das", "as", "dasdas", "da", "sdas", "dasdas", "das", "das", "da$", "ad$"};

        //then
        Assert.assertArrayEquals(expected,actual);
    }

    @Test
    public void spaceAfterPunctuationPass() {
        //given
        String str = "Rasd, asda,sadas, asdas,qwd.";

        //when
        String actual = thirdInstance.spaceAfterPunctuation(str);
        String expected = "Rasd, asda, sadas, asdas, qwd.";

        //then
        Assert.assertEquals(expected, actual);
    }

    @Test
    public void saveOnlyOneRepeatedCharactersPass() {
        //given
        String testString = "abbcccdddfffeeetttyyy";

        //when
        String actual = thirdInstance.saveOnlyOneRepeatedCharacters(testString).toString();
        String expected = "abcdfety";

        //then
        Assert.assertEquals(expected, actual);
    }

    @Test
    public void countNumberOfWordsPass() {
        //given
        String testString = "Кто понял, тот понял, а кто не понял, тот не понял.";

        //when
        int actual = thirdInstance.countNumberOfWords(testString);
        int expected = 11;

        //then
        Assert.assertEquals(expected,actual);
    }

    @Test
    public void deletePartOfStringPass() {
        //given
        String testString = "0123456789";

        //when
        String actual = thirdInstance.deletePartOfString(testString, 3, 5);
        String expected = "01789";

        //then
        Assert.assertEquals(expected, actual);
    }

    @Test
    public void reverseStringPass() {
        //given
        String testString = "qwertyuiop";

        //when
        String actual = thirdInstance.reverseString(testString);
        String expected = "poiuytrewq";

        //then
        Assert.assertEquals(expected, actual);
    }

    @Test
    public void deleteLastWordPass() {
        //given
        String testString = "aasdad asdad asds asdasd asd";

        //when
        String actual = thirdInstance.deleteLastWord(testString);
        String expected = "aasdad asdad asds asdasd";

        //then
        Assert.assertEquals(expected, actual);
    }
}
