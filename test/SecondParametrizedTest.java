import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;
import java.util.Arrays;

public class SecondParametrizedTest {

    @RunWith(Parameterized.class)
    public static class realNumberToStringParamTest{
        private double inputValue;
        private String expectedLine;

        public realNumberToStringParamTest(double value, String expected){
            inputValue = value;
            expectedLine = expected;
        }

    @Parameterized.Parameters(name = "ParameterizedTest")
    public static Iterable<Object[]> valuesForTest(){
        return Arrays.asList(new Object[][]{
                {2.6, "2.6"},
                {0.1, "0.1"},
                {1000000.3, "1000000.3"}
        });
        }

        @Test
        public void realNumberToStringParamTestPass(){
            Assert.assertEquals(expectedLine, new Second().realNumberToString(inputValue));
        }
    }

    //Testing next method integerToString()

    @RunWith(Parameterized.class)
    public static class integerToStringParamTest{
        private int inputValue;
        private String expectedLine;

        public integerToStringParamTest(int value, String expected){
            inputValue = value;
            expectedLine = expected;
        }

        @Parameterized.Parameters(name = "ParameterizedTest")
        public static Iterable<Object[]> valuesForTest(){
            return Arrays.asList(new Object[][]{
                    {2, "2"},
                    {0, "0"},
                    {1000000, "1000000"}
            });
        }

        @Test
        public void integerToStringParamTestPass(){
            Assert.assertEquals(expectedLine, new Second().integerToString(inputValue));
        }
    }


    //Testing next method stringToInteger()

    @RunWith(Parameterized.class)
    public static class stringToIntegerParamTest{
        private String inputLine;
        private int expectedValue;

        public stringToIntegerParamTest(String value, int expected){
            inputLine = value;
            expectedValue = expected;
        }

        @Parameterized.Parameters(name = "ParameterizedTest")
        public static Iterable<Object[]> valuesForTest(){
            return Arrays.asList(new Object[][]{
                    {"2", 2},
                    {"0", 0},
                    {"1000000", 1000000}
            });
        }

        @Test
        public void integerToStringParamTestPass(){
            Assert.assertEquals(expectedValue, new Second().stringToInteger(inputLine));
        }
    }


    //Testing next method stringToRealNumber()

    @RunWith(Parameterized.class)
    public static class stringToRealNumberParamTest{
        private String inputLine;
        private double expectedValue;

        public stringToRealNumberParamTest(String value, double expected){
            inputLine = value;
            expectedValue = expected;
        }

        @Parameterized.Parameters(name = "ParameterizedTest")
        public static Iterable<Object[]> valuesForTest(){
            return Arrays.asList(new Object[][]{
                    {"2.2", 2.2},
                    {"0.3", 0.3},
                    {"1000000.4", 1000000.4}
            });
        }

        @Test
        public void stringToRealNumberParamTestPass(){
            Assert.assertEquals(expectedValue, new Second().stringToRealNumber(inputLine), 0.0);
        }
    }


}
