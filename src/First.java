public class First {

    public static void main(String[] args) {
        First first = new First();

        System.out.println("Сейчас Вы увидите английский алфавит, выведенный большими буквами в одну строку:");
        System.out.println(first.showCapsAlphabet() + "\n");

        System.out.println("Сейчас Вы увидите английский алфавит, выведенный в обратном порядке в одну строку:");
        System.out.println(first.showReverseAlphabet() + "\n");

        System.out.println("Сейчас Вы увидите русский алфавит, выведенный маленькими буквами в одну строку:");
        System.out.println(first.showRussianAlphabet() + "\n");

        System.out.println("Сейчас Вы увидите цифры от 0 до 9, выведенные в одну строку:");
        System.out.println(first.showNumbersFromZeroToNine() + "\n");

        System.out.println("Сейчас Вы увидите ASCII-таблицу, выведенную в одну строку:");
        System.out.println(first.showASCIITable());


    }


    public String showCapsAlphabet() {
        StringBuilder alphabet = new StringBuilder();
        for (char i = 'A'; i <= 'Z'; i++) {
            alphabet.append(i + " ");
        }
        return alphabet.toString();
    }

    public String showReverseAlphabet() {
        StringBuilder reverseAlphabet = new StringBuilder();
        for (char i = 'z'; i >= 'a'; i--) {
            reverseAlphabet.append(i + " ");
        }
        return reverseAlphabet.toString();
    }

    public String showRussianAlphabet() {
        StringBuilder russianAlphabet = new StringBuilder();
        for (char i = 'а'; i <= 'я'; i++) {
            russianAlphabet.append(i + " ");
        }
        return russianAlphabet.toString();
    }

    public String showNumbersFromZeroToNine() {
        StringBuilder numbersFromZeroToNine = new StringBuilder();
        for (char i = '0'; i <= '9'; i++) {
            numbersFromZeroToNine.append(i + " ");
        }
        return numbersFromZeroToNine.toString();
    }

    public String showASCIITable() {
        StringBuilder tableASCII = new StringBuilder();
        for (char i = ' '; i < 274; i++) {
            tableASCII.append(i + " ");
        }
        return tableASCII.toString();
    }


}
