import java.util.Arrays;

public class Third {

    public int calculateLengthOfTheShortestWord(String inputLine) {
        String[] s = inputLine.split("[\\p{Punct}\\p{Space}]");
        int lengthShortestWord = s[0].length();
        for (String word : s) {
            if (word.length() != 0) {
                if (word.length() < lengthShortestWord) {
                    lengthShortestWord = word.length();
                }
            }
        }

        return lengthShortestWord;
    }


    public String[] replaceLastThreeCharactersWith$(String[] arrayWords, int wordLength) {
        for (int i = 0; i < arrayWords.length; i++) {
            if (arrayWords[i].length() == wordLength) {
                int temp = wordLength - 3;
                arrayWords[i] = arrayWords[i].substring(0, temp) + "$";
            }
        }
        return arrayWords;
    }

    public String spaceAfterPunctuation(String line) {
        line = line.replace(", ", ",");
        line = line.replace(",", ", ");

        return line;
    }

    public StringBuilder saveOnlyOneRepeatedCharacters(String line) {
        char[] chars = line.toCharArray();
        StringBuilder sb = new StringBuilder();
        boolean repeatedChar;
        for (int i = 0; i < chars.length; i++) {
            repeatedChar = false;
            for (int j = i + 1; j < chars.length; j++) {
                if (chars[i] == chars[j]) {
                    repeatedChar = true;
                    break;
                }
            }
            if (!repeatedChar) {
                sb.append(chars[i]);
            }
        }
        return sb;
    }

    public int countNumberOfWords(String line) {
        int count = 0;
        String[] temp = line.split("[\\p{Space}]");
        for (String splitter : temp) {
            count++;
        }

        return count;
    }

    public String deletePartOfString(String line, int indexOf, int countCharacter) {
        indexOf = indexOf - 1;
        int indexFrom = indexOf + countCharacter;
        StringBuffer sb = new StringBuffer(line);
        sb.replace(indexOf, indexFrom, "");

        return sb.toString();
    }

    public String reverseString(String line) {
        StringBuffer stringBuffer = new StringBuffer(line);
        stringBuffer.reverse();

        return stringBuffer.toString();
    }


    public String deleteLastWord(String line) {
        String[] str = line.split("[\\p{Space}]");
        str[str.length - 1] = " ";
        line = Arrays.toString(str);
        line = line.replace(", ", " ").replace("[", "").replace("]", "").trim();

        return line;
    }
}
