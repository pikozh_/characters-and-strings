public class Second {

    public static void main(String[] args) {
        Second second = new Second();
        System.out.println("Сейчас Вы увидите преобразование целого  числа 1 в строку:");
        System.out.println(second.integerToString(1)+"\n");

        System.out.println("Сейчас Вы увидите преобразование вещественного числа 1.1 в строку:");
        System.out.println(second.realNumberToString(1.1)+"\n");

        System.out.println("Сейчас Вы увидите преобразование строки \"123\" в целое число:");
        System.out.println(second.stringToInteger("123")+"\n");

        System.out.println("Сейчас Вы увидите преобразование строки \"123.123\" в вещественное число:");
        System.out.println(second.stringToRealNumber("123.123")+"\n");
    }

    public String integerToString(int input){
        return String.valueOf(input);
    }

    public String realNumberToString(double input){
        return String.valueOf(input);
    }

    public int stringToInteger(String input){
        return Integer.parseInt(input);
    }

    public double stringToRealNumber(String input){
        return Double.parseDouble(input);
    }


}
